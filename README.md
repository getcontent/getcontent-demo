# GetContent Installation

## Initiate a new Laravel instance (or add to an existing one)
```
laravel new getcontent-demo
cd getcontent-demo
composer require milkmedia/getcontent
php artisan vendor:publish --tag=public --provider="MilkMedia\GetContent\GetContentServiceProvider"
php artisan make:auth #if required
php artisan migrate
```

GetContent uses Laravel’s API token for authentication so the `users` table needs to have an `api_token` field:

```
$table->string('api_token', 60)->unique();
```

**Note: when users are added they will need to be assigned a 60 character token** 

## Add to  `routes/web.php`

```
use MilkMedia\GetContent\GetContent;

Route::middleware('auth')->group(function () {
    GetContent::editorRoutes();
});

GetContent::webRoutes();
```

## Add to `routes/api.php`

```
use MilkMedia\GetContent\GetContent;

Route::middleware('auth:api')->group(function () {
    GetContent::apiRoutes();
});
```
